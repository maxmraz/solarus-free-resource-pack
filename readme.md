# Creative Commons resource pack for Solarus

This repository provides free
musics, sounds, tilesets, sprites and scripts
for the
[Solarus engine](https://github.com/solarus-games/solarus).

You can use these resources if you want to develop a game with the
Solarus engine using free (as in freedom) assets.

- All data files that we created (other than Lua scripts) are licensed under
Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0).
http://creativecommons.org/licenses/by-sa/4.0/

- Lua scripts are licensed under the terms of the GNU General Public License
in version 3.

## Branches

Branch master always points to the latest release of this resource pack.

The latest resource pack release is always compatible
with the latest Solarus version.

Resources compatible with older versions or development versions of Solarus
live in their own branches.

## Importing resources from this pack into an existing quest

When you create a new quest with
[Solarus Quest Editor](https://github.com/solarus-games/solarus-quest-editor),
your data are initialized with a subset of free resources from this pack.

You can import more content from this pack into your quest if you want.
Here is how to proceed:

- Make a backup of your quest.
- Open your quest with
  [Solarus Quest Editor](http://www.solarus-games.org/development/quest-editor]).
- In the File menu, click "Import from a quest".
- Select this resource pack as the source quest.
- Select the files you want to want to import and click the Import button.
